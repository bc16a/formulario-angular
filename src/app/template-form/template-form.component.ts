import { Component, OnInit } from '@angular/core';
import { Usuarios } from '../models/usuario';
import { UsuarioService } from '../services/usuarioService';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.scss']
})
export class TemplateFormComponent implements OnInit {
  form: any;


  onSubmit(form){
    console.log(form)

   // console.log(this.usuario)
    this.dataService.addServico(this.usuario);
   
  }

  constructor(public usuario : Usuarios, public dataService: UsuarioService) { }

  ngOnInit() {
  }

}
