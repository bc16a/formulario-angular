import { Component, OnInit } from '@angular/core';
import { Usuarios } from '../models/usuario';
import { UsuarioService } from '../services/usuarioService';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: ['./data-form.component.scss']
})

export class DataFormComponent implements OnInit {
  usaurios : Usuarios[] =[];

  constructor(public usuario : Usuarios, public dataService: UsuarioService) { }

  ngOnInit(): void {

    this.dataService.getAllServicos().subscribe(usaurios => {
      console.log(usaurios)
      this.usaurios = usaurios;
      },
      (error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
      });

  }

}
