import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Usuarios} from '../models/usuario';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class UsuarioService {
  
  private readonly API_URL = 'http://localhost:3000/usaurios';
  

  dataChange: BehaviorSubject<Usuarios[]> = new BehaviorSubject<Usuarios[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Usuarios[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllServicos(){
   return this.httpClient.get<Usuarios[]>(this.API_URL);
  }


 //REAL LIFE CRUD Methods I've used in my projects. ToasterService uses Material Toasts for displaying messages:

    // ADD, POST METHOD
    addServico(servico: Usuarios): void {
    this.httpClient.post(this.API_URL, servico, httpOptions).subscribe((servico: Usuarios)=> {
      console.log('Serviço Add', servico)
      this.dialogData = servico;
      },
      (err: HttpErrorResponse) => {
    });
   }


   

}



